##Prueba tecnica React, Redux, SASS

### Instalar
    Ejecute en un terminal : 
    -git clone https://gitlab.com/chinosan/react-prueba-tecnica
    -cd /react-prueba-tecnica
    -npm install
    -npm run dev

    ###Capturas de pantall
    ![captura de pantalla](./screenshots/img2.png)

<div>
    <img src="screenshots/img1.png" alt="Logo" width="100%" >
    <img src="screenshots/img2.png" alt="Logo" width="100%" >
    <img src="screenshots/img3.png" alt="Logo" width="100%" >
</div>

    ***Existe una version alternativa en la rama "fixed", que cuenta con redux y sass, pendiente jest... ***