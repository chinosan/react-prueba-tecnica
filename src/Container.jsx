import { useState, useEffect } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import Table from './components/table'
import { useNavigate } from 'react-router-dom'
import Details from './components/Details'

const Container = ()=> {
  const [page, setPage] = useState(1)
  const [start, setStart] = useState(0)
  const [end, setEnd] = useState(10)
  const [datos, setDatos] = useState([])
  const [datosXpag, setDatosXpag] = useState([])
  const link = "http://127.0.0.1:5173/"
  const navigate = useNavigate()
  const [item, setItem] = useState({})

  const getData = async() =>{
    const result = await fetch("https://api.datos.gob.mx/v1/condiciones-atmosfericas")
    const data = await result.json()
    const {results} = data
    console.log(data.results)
    setDatos([...data.results])
    setDatosXpag([...data.results.slice(0,10)])
  }
  useEffect(() => {
    getData()
  }, [])

  const handleNext = () => {
    if(page==11) return;
    setEnd(end+(10*page))
    setStart(start+(10*page))
    setDatosXpag(datos.slice((10*page),(10*page+10)))
  }
  

  return (
    <div className="App">
      <table>
        <thead>
          <th>_id</th>
          <th>cityid</th>
          <th>name</th>
          <th>state</th>
          <th>probabilityofprecip</th>
          <th>relativehumidity</th>
          <th>lastreporttime</th>
        </thead>
        <tbody>

          </tbody>
          </table>
        
      {
        datos.length==0
        ?<h1>Sin resultados</h1>
        :datosXpag.map( (reg)=> <tr key={reg._id} 
          style={{display:"flex", justifyContent:"space-between"}}
        >
          
          <td onClick={()=>setItem(reg)} >{reg._id}</td>
          <td>{reg.cityid}</td>
          <td>{reg.name}</td>
          <td>{reg.state}</td>
          <td>{reg.probabilityofprecip}</td>
          <td>{reg.relativehumidity}</td>
          <td>{reg.lastreporttime}</td>
          <td>{
              reg.probabilityofprecip>60 || reg.relativehumidity>50
              ? "LLueve"
              : "No llueve"
            }</td>
        </tr> )
      }
      <h2>Total de registros: {datos.length}</h2>
      {/* <Table data={datos}  /> */}
      <button onClick={()=>{setPage(page+1);handleNext()}}>Siguiente </button>

      <Details data={item} />

    </div>
    
  )
}

export default Container
