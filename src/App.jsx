import React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Details from './components/Details'
import Container from './Container'
// import store from './context/store'

function App() {
  return (
    // <Provider store={store} >
      <div className='App' >
        <BrowserRouter>
          <Routes>
            <Route path='/' element={<Container />} />
            <Route path="/details" element={<Details />} />
          </Routes>
        </BrowserRouter>
      </div>

    // </Provider>
  )
}

export default App